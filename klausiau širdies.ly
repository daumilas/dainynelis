\version "2.16.2"
#(set-default-paper-size "a5")
#(set-global-staff-size 17)
\paper {
  left-margin = 1\cm
  top-margin = 1.5\cm
}

\header {
  title = "Klausiau širdies"
  composer = "Sean Francis Conway ir kiti"
  opus = "2013"
  tagline = ""
}

global = {
  \key a \major
  \numericTimeSignature
  \time 3/4
  \partial 4
}

sopranoVoice = \relative c' {
  \global
  \dynamicUp
  e8 (a8) a2 fis4 fis2 a4 b2 cis8 (b) a2
  a4 cis2 e4 cis2 e4 fis8 (e4.) cis4 b8 (a4.) fis4 e2\fermata
  e8 (a8) a2 fis4 fis2 a4 b2 cis8 (b) a2.
}

verseLT = \lyricmode {
  Klau -- siau šir -- dies: "\"Ar" tu dar "čia?\""
  At -- sa -- kė ji: "\"Juk" mei -- lė am -- ži -- "na.\""
  Ji am -- ži -- na, ji am -- ži -- na.
}

verseEN = \lyricmode {
    I asked my heart, "\"are" you still "there?\""
    My heart told me "\"no" love will ne -- ver "die.\""
    No ne -- ver die, no ne -- ver die.
}

altoVoice = \relative c'' {
  \global
  \dynamicUp
  a8 (e) e2 a4 gis8 (fis4.) e4 gis2 a8 (gis) fis2
  fis4 a2 gis4 a2 gis4 gis2 gis4 gis2 gis4 e2\fermata
  a8 (e) e2 a4 gis8 (fis4.) e4 gis2 a8 (e8) e2.
}


tenorVoice = \relative c' {
  \global
  \dynamicUp

  a8 (cis) cis2 cis4 a2 a4 b2 cis8 (e) fis (cis4.)
  d4 e2 e4 cis2 cis4 b2 b4 b2 b4 b2\fermata
  a8 (cis) cis2 cis4 a2 a4 b2 cis8 (e) e2. \bar "|."
}

bassVoice = \relative c {
  \global
  \dynamicUp
  e8 (cis) a2 cis4 e8 (fis4.) a4 e2 e4 fis2
  d4 a'2 gis4 fis2 e4 e2 e4 e2 e4 e2\fermata
  e8 (cis) a2 cis4 e8 (fis4.) a4 e2 e4 <a a,>2.
}


sopranoVoicePart = \new Staff \with {
  instrumentName = "Sopranas"
  midiInstrument = "choir aahs"
} { \sopranoVoice }
\addlyrics { \verseLT }
\addlyrics { \verseEN }

altoVoicePart = \new Staff \with {
  instrumentName = "Altas"
  midiInstrument = "choir aahs"
} { \altoVoice }

tenorVoicePart = \new Staff \with {
  instrumentName = "Tenoras"
  midiInstrument = "choir aahs"
} { \clef "treble_8" \tenorVoice }
\addlyrics { \verseLT }
\addlyrics { \verseEN }

bassVoicePart = \new Staff \with {
  instrumentName = "Bosas"
  midiInstrument = "choir aahs"
} { \clef bass \bassVoice }

\score {
  <<
    \sopranoVoicePart
    \altoVoicePart
    \tenorVoicePart
    \bassVoicePart
  >>
  \layout { }
  \midi {
    \context {
      \Score
      tempoWholesPerMinute = #(ly:make-moment 100 4)
    }
  }
}
