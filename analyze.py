# -*- coding: utf-8 -*-

import os, re, io, random

def toOneFile():
    files = list(filter(lambda f: f[-4:] == ".tex" and f != "all.tex", os.listdir(".")))
    s = ""
    for f in files:
        s += io.open(f, mode="r", encoding="utf-8").read()
    return s

def removeChords(text):
    return re.sub("[\{\(](.{0,5})[\}\)]", r'\1', re.sub("\[[^\]]*\]", "", text))

texts = removeChords(toOneFile())
f = io.open("all.tex", mode="w", encoding="utf-8")
f.write(texts)
f.close()

songs = texts.replace("\n", " ").split("\\song")

def names(songs):
    return list(map(lambda x: re.search(r'\{([^\}]*)\}', x).group(1), songs))


def listNamesWith(r):
    print ("\n".join(sorted(list(set(names(list(filter(lambda s: re.search(r, s, re.IGNORECASE|re.MULTILINE), songs))))))).encode("utf-8"))

def countNames(r):
    return len(set(names(list(filter(lambda s: re.search(r, s, re.IGNORECASE|re.MULTILINE), songs)))))

#listNamesWith(r"meil|myl")

#listNamesWith(r"(?<!pa)saul")

#listNamesWith(r"pasaul")

#print countNames(ur"med[iįežy]")

#print countNames(ur"med[uaų]")

print (listNamesWith(r'(?=.*(meil|myl))(?=.*(?<!pa)saul)'))
